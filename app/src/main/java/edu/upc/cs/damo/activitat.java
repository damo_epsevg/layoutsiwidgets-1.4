// 1.4

// Un text fix i un text editable, amb Linearlayout


// Modifiquem l'orientació del layout, i la mida i el color del text



package edu.upc.cs.damo;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.LinearLayout;


public class activitat extends Activity {

	static String SALUTACIO = "Hola a tothom";
	TextView text;
	EditText camp;
	LinearLayout layout;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Construim els elements a visualitzar
        
        text = new TextView(this);
    	text.setText(SALUTACIO);
    	
    	camp = new EditText(this);
    	
    	// Introduim els elements en un contenidor
    	layout = new LinearLayout(this);
     	layout.addView(text);
     	layout.addView(camp);
     	
     	// Canviem l'orientació
     	layout.setOrientation(LinearLayout.VERTICAL);
    
     	// Demanem al camp que ocupi més espai
     	camp.setWidth(200);
     	text.setTextColor(Color.GREEN);
  
        setContentView(layout);
    }
}
